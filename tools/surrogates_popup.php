<?
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Web\Uri;
use Uplab\Editor\Surrogates;
use Uplab\Editor\Tools;


/**
 * TODO: избавиться от этого файла, разнести всю логику по компонентам
 *
 * @noinspection PhpIncludeInspection
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";


Loader::includeModule("fileman");
Loader::includeModule("uplab.editor");


$type = $_REQUEST["TYPE"];
$isSectionType = $type == "SECTION";
$isElementType = $type == "ELEMENT" || !$type;
$isGalleryType = $type == "GALLERY";


$hasFlex = Loader::includeModule("uplab.flexiblock");
if ($hasFlex) {
	if (!$type) {
		$isElementType = false;
		$isFlexType = true;
	} else {
		$isFlexType = $type == "FLEXIBLOCK";
	}
}


if ($isSectionType) {
	$searchUrl = "/bitrix/admin/iblock_section_search.php";
	$componentName = "snippet.section";
} elseif ($isElementType) {
	$searchUrl = "/bitrix/admin/iblock_element_search.php";
	$componentName = "snippet.element";
} elseif ($isGalleryType) {
	$searchUrl = false;
	$componentName = "snippet.gallery";
} elseif ($isFlexType) {
	$searchUrl = false;
	$componentName = "snippet.flexiblock";
}


$template = htmlspecialchars($_REQUEST["TEMPLATE"]) ?: "";
$componentTemplates = Surrogates::getTemplatesList(["uplab.editor", $componentName]);


if (!is_array($_REQUEST["ID"])) {
	$idList = preg_split("~\s*,\s*~", $_REQUEST["ID"]);
} else {
	$idList = $_REQUEST["ID"];
}
$idList = array_filter(array_map("intval", $idList));


$code = $_REQUEST["CODE"];


$arItems = [];
if (!empty($idList)) {
	if (!Loader::includeModule("iblock")) return;
	$arOrder = ["sort" => "asc", "date_active_from" => "desc"];
	$arFilter = [
		"ID" => $idList,
	];
	$arSelect = ["ID", "IBLOCK_ID", "NAME"];

	if ($isSectionType) {
		$res = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
	} else {
		$res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
	}

	while ($item = $res->Fetch()) {
		$arItems[$item["ID"]] = $item;
	}
}


if (array_key_exists("ADD", $_REQUEST) || count($idList) < 1) {
	$idList[] = [];
}


?>


<form class="uplab-surrogate-editor-form" action="">

	<label>
		<select name="TYPE" style="width: 100%;" data-form-input>
			<? if ($hasFlex): ?>
				<option value="FLEXIBLOCK"
					<?= $isFlexType ? "selected" : "" ?> >Быстрый инфоблок (uplab.flexiblock)
				</option>
			<? endif; ?>
			<option value="ELEMENT"
				<?= $isElementType ? "selected" : "" ?> >Элемент инфоблока
			</option>
			<option value="SECTION"
				<?= $isSectionType ? "selected" : "" ?> >Раздел инфоблока
			</option>
			<option value="GALLERY"
				<?= $isGalleryType ? "selected" : "" ?>>Галерея
			</option>
		</select>
	</label>

	<? if (!$isFlexType): ?>
		<hr style="margin: 20px 0;"/>
	<? else: ?>
		<div style="padding: 2px 0;"></div>
	<? endif; ?>

	<? foreach ($idList as $i => $id): ?>
		<?
		$item = $arItems[$id];
		$inp["n"] = "UPLAB_SNP";
		$inp["k"] = randString(3);

		if ($isSectionType) {
			$editLink = Tools::buildAdminSectionLink($item["IBLOCK_ID"], $item["ID"]);
		} elseif ($isElementType) {
			$editLink = Tools::buildAdminElementLink($item["IBLOCK_ID"], $item["ID"]);
		} else {
			$flexAttrs = "style='display: flex; justify-content: flex-end;'";
		}
		?>

		<? if ($i === 0) {
			if ($isSectionType) {
				include __DIR__ . "/__inc_popup_section_header.php";
			} elseif ($isElementType) {
				include __DIR__ . "/__inc_popup_element_header.php";
			}
		} ?>

		<div class="uplab-surrogate-row" <?= $flexAttrs; ?>>
			<? if ($isElementType || $isSectionType) { ?>
				<div class="uplab-surrogate-col-1">
					<?
					echo "<input " .
						"   name=\"ID[]\" " .
						"   id=\"{$inp["n"]}[{$inp["k"]}]\" " .
						"   data-form-input " .
						"   size=\"5\" " .
						"   value=\"{$item["ID"]}\" " .
						"   type=\"text\" >";

					echo "&nbsp;";

					$url = (new Uri($searchUrl))->addParams([
						"lang" => LANGUAGE_ID,
						"n"    => $inp["n"],
						"k"    => $inp["k"],
					])->getUri();

					echo "<input " .
						"   type=\"button\" " .
						"   value=\"...\" " .
						"   onclick=\"jsUtils.OpenWindow('{$url}', 900, 700);\">";
					?>
				</div>
				<div class="uplab-surrogate-col-2">
					<span id="sp_<?= md5($inp["n"]) ?>_<?= $inp["k"] ?>" style=""><?= $item["NAME"] ?: "" ?></span>
				</div>
			<? } elseif ($isFlexType) { ?>
				<style>
                    #up-editor-append {
                        display: none;
                    }
				</style>
				<script><?= file_get_contents(
						Application::getDocumentRoot() .
						"/local/modules/uplab.flexiblock/include/js/init.js"
					) ?></script>
				<? $APPLICATION->IncludeComponent(
					"uplab.flexiblock:content.block.config",
					"visual.editor",
					array(
						"FIELD_CODE" => $code,
						"CONTEXT_ID" => "V",
						"ENTITY_ID"  => "VISUAL_EDITOR",
					),
					false,
					array("HIDE_ICONS" => "Y")
				); ?>
			<? } else { ?>
				<?
				echo \Bitrix\Main\UI\FileInput::createInstance(array(
					"name"        => "MEDIALIBRARY_PICTURES[]",
					"description" => false,
					"upload"      => false,
					"medialib"    => true,
					"fileDialog"  => false,
					"cloud"       => false,
					"delete"      => true,
				))->show($_REQUEST["MEDIALIBRARY_PICTURES"], true);
			} ?>
			<? if ($item["IBLOCK_ID"]): ?>
				<div class="uplab-surrogate-col-3">
					<a href="<?= $editLink ?>"
					   target="_blank"
					   class="uplab-surrogate-btn"><?= file_get_contents(
							$_SERVER["DOCUMENT_ROOT"] .
							"/bitrix/images/uplab.editor/i-pencil.svg"
						) ?></a>
				</div>
			<? endif; ?>
		</div>
	<? endforeach; ?>

	<? if (!$isFlexType): ?>
		<hr style="margin: 20px 0;"/>
	<? else: ?>
		<div style="padding: 12px 0;"></div>
	<? endif; ?>

	<label>
		<span style="display: block;margin-bottom: 10px;">
			Выберите шаблон для отображения:
		</span>

		<br>

		<select name="TEMPLATE" style="width: 100%;">
			<? foreach ($componentTemplates as $tpl): ?>
				<option value="<?= $tpl["NAME"] ?>"
					<?= $template == $tpl["NAME"] ? "selected" : "" ?>
				><?= $tpl["DISPLAY_NAME"] ?></option>
			<? endforeach; ?>
		</select>
	</label>


</form>
