export default class UplabSurrogatesDialog {

    constructor(_editor, _params, _tag) {
        this._editor = _editor;
        this._params = _params;

        // если мы передали тег в конструктор, то получаем параметры тега
        // там у нас в json хранится содержание всего аккордеона
        this._tag = _tag || {};
        this._tag.params = this._tag.params || {};
        this._tag.params.data = this._tag.params.data || {};
        this._tag.params.data.attributes = this._tag.params.attributes || {};
        this.data = this._tag.params.data || {};

        this.initDialog();
    }

    initDialog() {
        const self = this;
        let html;

        self._dialog = new BX.CDialog({ //Инициализация окна добавления/редактирования аккордеона
            title: self._params.dialogTitle,
            // min_width: 400,
            // min_height: 400,
            icon: 'head-block',
            resizable: false,
            width: 520,
            height: 510,
            content_url: self._params.getDialogUrl(self.data.attributes),
            buttons: [
                // Кнопки в окне

                { // Кнопка применяет изменения или добавляет аккордеон
                    title: "OK",
                    name: 'saveUplabSurrogate',
                    className: 'adm-btn-save',
                    id: 'saveUplabSurrogate',
                    action: function () {
                        var _thisBtn = this; // Сама кнопка
                        html = self.getSnippetHTML(); //Метод получает html аккордеона

                        if (self._tag.params.html) {
                            self._tag.params.html = html;  // заменить контент, если сниппет уже есть,
                        } else {
                            console.log('insert', html);
                            self._editor.selection.InsertHTML(html); // вставить контент в редактор
                        }

                        setTimeout(function () {
                            // Спустя немного времени обновляем содержимое
                            // чтобы применился новый аккордеон
                            self._editor.synchro.FullSyncFromIframe();
                        }, 50);

                        this.parentWindow.Close(); //Закрываем окно
                        $(this.parentWindow.DIV).remove(); //Удаляем окно из DOM-a
                    }
                },

                { //Кнопка добавляем блок для редактирования элемента аккордеона
                    title: "Еще элемент",
                    id: 'up-editor-append',
                    className: "adm-btn-add",
                    action: function () {
                        const $form = $('.uplab-surrogate-editor-form');
                        $.get(
                            uplabEditorSnippetsParam.dialogUrl + '?ADD',
                            $form.serializeArray(),
                            function (res) {
                                $form.parent().html(res);
                            }
                        );
                    }
                },

                { //Отмена
                    title: BX.message('JS_CORE_WINDOW_CANCEL'),
                    id: 'up-editor-cancel',
                    name: 'cancel',
                    action: function () {
                        this.parentWindow.Close(); //Закрываем окно
                        $(this.parentWindow.DIV).remove(); //Удаляем DOM окна
                    }
                }
            ]
        });
    }

    getContent() {
        // Метод получает содержимое окна редактора аккордеона
        // Заменено на ссылку
    }

    refreshContent() { //Метод обновляет содержимое окна редактора аккордеона
        const self = this;
        self._dialog.SetContent(self.getContent());
    }

    getSnippetHTML() { //Метод получает html аккордеона по данным из this.data
        const self = this;
        let html = self._params.htmlTpl;

        const $form = $('.uplab-surrogate-editor-form');
        const $disabled = $form.find('[disabled]');

        $disabled.prop('disabled', false);
        let formData = $form.serializeArray();
        $disabled.prop('disabled', true);

        let data = {};
        let values = {};

        $.each(formData, function (key, value) {
            let name = value.name.match(/((.+)\[([\w\d_-]*)]|.+)/);
            let isDeleteAction = value.name.match(/[\d]{1,2}_del/g);

            if (!value.value) return;

            if (name[1] === 'TYPE') {
                data[name[1]] = value.value;
            } else if(data['TYPE'] === 'GALLERY') {
                if (!values['MEDIALIBRARY_PICTURES']) values['MEDIALIBRARY_PICTURES'] = [];
                if (isDeleteAction !== null) { //это условие если выполняется, то оно бывает на последнем элементе массива
                    let indexToDelete = value.name.replace('_del','');
                    delete values['MEDIALIBRARY_PICTURES'][indexToDelete];

                    //очищаем от значений типа null
                    values['MEDIALIBRARY_PICTURES'] = values['MEDIALIBRARY_PICTURES'].filter(function (el) {
                        return el != null;
                    });
                } else {
                    if (value.name === 'MEDIALIBRARY_PICTURES[]') {
                        let lastIndex = values['MEDIALIBRARY_PICTURES'].length ? values['MEDIALIBRARY_PICTURES'].length : 0;
                        values['MEDIALIBRARY_PICTURES'][lastIndex] = value.value;
                    } else if (value.name === 'TEMPLATE') {
                        values['TEMPLATE'] = value.value;
                    } else {
                        values['MEDIALIBRARY_PICTURES'][value.name] = value.value;
                    }
                }
            } else {
                if (name[2]) {
                    if (name[3]) {
                        values[name[2]][name[3]] = value.value;
                    } else {
                        values[name[2]] = values[name[2]] || [];
                        values[name[2]].push(value.value);
                    }
                } else {
                    values[name[1]] = value.value;
                }
            }
        });

        html = html.replace(/#TYPE#/g, data['TYPE'] || '');
        html = html.replace(/#VALUES#/g, JSON.stringify(values || ''));

        return html;
    }

    show() { //Метод показывает окно редактора аккордеона
        this._dialog.Show();
    }

}
