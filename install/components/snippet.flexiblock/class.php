<?

namespace Uplab\Editor\Components;

use Bitrix\Main\Loader;
use CBitrixComponent;
use Exception;
use Uplab\FlexIblock\Content\ContentBlock;
use Uplab\FlexIblock\Content\ContentBlockTable;
use Uplab\FlexIblock\Content\ValueTable;
use Uplab\FlexIblock\Context\ContextController;


if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


class SnippetFlexIblockComponent extends CBitrixComponent
{
	protected $cacheKeys = [
		"ID",
		"NAME",
		"__RETURN_VALUE",
	];

	protected $dependModules = ["uplab.editor", "uplab.flexiblock"];

	/**
	 * дополнительные параметры, от которых должен зависеть кеш
	 *
	 * @var array
	 */
	protected $additionalCacheID = [];

	/** @noinspection PhpMissingReturnTypeInspection */
	public function onPrepareComponentParams($params)
	{
		if ($params["DESIGN_MODE"]) {
			$params["CACHE_TYPE"] = "N";
			$params["CACHE_TIME"] = 0;
		} else {
			if (!isset($params["CACHE_TYPE"])) {
				$params["CACHE_TYPE"] = "A";
			}

			if (!isset($params["CACHE_TIME"])) {
				if (defined("CACHE_TIME")) {
					$params["CACHE_TIME"] = CACHE_TIME;
				} else {
					$params["CACHE_TIME"] = 360000;
				}
			}
		}

		return array_filter($params);
	}

	public function executeComponent()
	{
		try {
			$this->executeProlog();
			$this->__includeComponent();

			if (!$this->readDataFromCache()) {
				$this->putDataToCache();

				$this->getResult();
				$this->includeComponentTemplate();

				$this->endResultCache();

			}

			$this->executeEpilog();

			return $this->arResult["__RETURN_VALUE"];
		} catch (Exception $e) {
			$this->abortResultCache();
			ShowError($e->getMessage());
		}

		return false;
	}

	/**
	 * определяет читать данные из кеша или нет
	 *
	 * @return bool
	 */
	protected function readDataFromCache()
	{
		if ($this->arParams["CACHE_TYPE"] == "N") {
			return false;
		}

		return !($this->StartResultCache(false, $this->additionalCacheID));
	}

	/**
	 * кеширует ключи массива arResult
	 */
	protected function putDataToCache()
	{
		if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0) {
			$this->SetResultCacheKeys($this->cacheKeys);
		}
	}

	/**
	 * выполняет действия перед кешированием
	 */
	protected function executeProlog()
	{
	}

	/**
	 * выполняет действия после выполнения компонента, например установка заголовков из кеша
	 */
	protected function executeEpilog()
	{
	}

	protected function getResult()
	{
		if (!Loader::includeModule("uplab.flexiblock")) return;

		// dump(["arResult" => $this->arResult, "arParams" => $this->arParams, $this->getTemplateName()]);

		$code = $this->arParams["CODE"] ?? "";
		if (!$code) return;

		$contentBlock = ContentBlockTable::getByParameters(
			ContextController::VISUAL_EDITOR,
			ContextController::VISUAL_EDITOR_ENTITY_ID,
			$code
		);

		if ($contentBlock) {
			$this->arResult = $contentBlock->getPreparedValues();
			$this->arResult["CONTENT_BLOCK"] = $contentBlock;

			if (empty($this->arResult["VALUE"])) {
				$this->arResult["VALUE"] = $this->arResult["VALUES"][0] ?? [];
			}
		}
	}

}
