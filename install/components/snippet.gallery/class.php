<?
use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use \Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


class UPLAB_EDITOR_SNIPPET_GALLERY_COMPONENT extends CBitrixComponent
{
	/** @var ErrorCollection */
	protected $errorCollection;

	/**
	 * @var string[]
	 */
	protected $requiredModules = [
		'fileman', 'iblock'
	];

	/**
	 * Base constructor.
	 *
	 * @param CBitrixComponent|null $component Component object if exists.
	 */
	public function __construct($component = null)
	{
		parent::__construct($component);
		$this->errorCollection = new ErrorCollection();
	}

	public function onPrepareComponentParams($arParams): array
	{
		$arParams['CACHE_TYPE'] = $arParams['CACHE_TYPE'] ?? 'A';
		$arParams['CACHE_TIME'] = $arParams['CACHE_TIME'] ?? 3600000;

		return $arParams;
	}

	/**
	 * Первоначальная проверка обязательных данных и подключение модулей
	 *
	 * @return bool
	 * @throws \Bitrix\Main\LoaderException
	 */
	protected function firstSettingsAndAccess(): bool
	{
		$res = true;

		//Проверка подключения необходимых модулей для работы
		if ($this->requiredModules) {
			foreach ($this->requiredModules as $requiredModule) {
				if (!Loader::includeModule($requiredModule)) {
					$this->addError("Module `{$requiredModule}` is not installed.");
					$res = false;
				}
			}
		}
		CMedialib::Init();
		return $res;
	}

	/**
	 * Получаем данные, кешируем и записываем в массив $arResult
	 */
	protected function getResult(): void
	{
		global $DB;
		//Цикл для получения ID файлов по их абсолютному пути

		$directorySeparator = (defined('DIRECTORY_SEPARATOR') && DIRECTORY_SEPARATOR) ? DIRECTORY_SEPARATOR : '/';
		//Todo upload может быть в облаке. Что делать с этим?
		$uploadDir = $directorySeparator . \Bitrix\Main\Config\Option::get('main', 'upload_dir', 'upload') . $directorySeparator;
		$uploadDirLength = strlen($uploadDir);

		if ($uploadDirLength > 0 && $items = $this->arParams['~MEDIALIBRARY_PICTURES']) {

			foreach ($items as $mediaLibraryPicture) {

				$path = substr($mediaLibraryPicture, $uploadDirLength);
				$key = strripos($path, $directorySeparator);
				if ($key !== false) {
					$fileName = substr($path, $key + 1);
					$path = substr($path, 0, $key);

					$resFilter = [
						'SUBDIR'    => $path,
						'FILE_NAME' => $fileName
					];
					$res = \Bitrix\Main\FileTable::getList([
						'select' => [
							'ID'
						],
						'filter' => $resFilter,
						'order'  => [
							'ID' => 'DESC'
						],
						'limit'  => 1
					]);
					if ($row = $res->fetch()) {
						$this->arResult['ITEMS'][$row['ID']]['src'] = $mediaLibraryPicture;
					}
				}
			}
			//Считываем описание для картинок
			if ($this->arResult['ITEMS']) {
				$db_res = $DB->Query("SELECT SOURCE_ID, DESCRIPTION FROM b_medialib_item WHERE SOURCE_ID IN (" . implode(',', array_keys($this->arResult['ITEMS'])) . ")");
				while ($arRes = $db_res->Fetch()) {
					$this->arResult['ITEMS'][$arRes['SOURCE_ID']]['description'] = $arRes['DESCRIPTION'];
				}
			}
		}
	}

	public function executeComponent(): void
	{
		Loc::loadMessages(__FILE__);
		try {
			if ($this->firstSettingsAndAccess()) {
				$this->getResult();
				$this->includeComponentTemplate();
			}
		} catch (Exception $exception) {
			$this->abortResultCache();
			$this->addError($exception->getMessage());
		}
		$this->processErrors();
	}
	/**
	 * Errors processing depending on error codes.
	 *
	 * @return bool
	 */
	protected function processErrors(): bool
	{
		if (!empty($this->errorCollection)) {
			/** @var Error $error */
			foreach ($this->errorCollection as $error) {
				ShowError($error->getMessage());
			}
		}

		return false;
	}

	/**
	 * Return true if errors exist.
	 *
	 * @return bool
	 */
	protected function hasErrors(): bool
	{
		return (bool)count($this->errorCollection);
	}

	/**
	 * Add error
	 *
	 * @param string $message
	 * @param int    $code
	 */
	protected function addError(string $message, $code = 0): void
	{
		$this->errorCollection->setError(new \Bitrix\Main\Error($message, $code = 0));
	}
}
